#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int sharpenFilter[3][3] = {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}};
int avgFilter[3][3] = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
int LP2Filter[3][3] = {{1, 1, 1}, {1, 4, 1}, {1, 1, 1}};
int gaussFilter[3][3] = {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}};
int HP1Filter[3][3] = {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}};
int horizontalFilter[3][3] = {{0, 0, 0}, {-1, 1, 0}, {0, 0, 0}};
int gradientFilter[3][3] = {{-1, 1, 1}, {-1, -2, 1}, {-1, 1, 1}};
int embossingFilter[3][3] = {{-1, 0, 1}, {-1, 1, 1}, {-1, 0, 1}};

typedef struct
{
    unsigned char red, green, blue;
} PPMPixel;

typedef struct
{
    int x, y;
    PPMPixel *data;
} PPMImage;

#define RGB_MAX_COMPONENT_COLOR 255

static PPMImage *readPPM(const char *filename)
{
    char buff[16];
    PPMImage *img;
    FILE *fp;
    int c, rgb_comp_color;
    //open PPM file for reading
    fp = fopen(filename, "rb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
    {
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6')
    {
        fprintf(stderr, "Invalid image format (must be 'P6')\n");
        exit(1);
    }

    //alloc memory for image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#')
    {
        while (getc(fp) != '\n')
            ;
        c = getc(fp);
    }

    ungetc(c, fp);
    //read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
    {
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1)
    {
        fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color != RGB_MAX_COMPONENT_COLOR)
    {
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n')
        ;
    //memory allocation for pixel data
    img->data = (PPMPixel *)malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y)
    {
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}
void writePPM(const char *filename, PPMImage *img)
{
    FILE *fp;
    //open file for output
    fp = fopen(filename, "wb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //write the header file
    //image format
    fprintf(fp, "P6\n");

    //image size
    fprintf(fp, "%d %d\n", img->x, img->y);

    // rgb component depth
    fprintf(fp, "%d\n", RGB_MAX_COMPONENT_COLOR);

    // pixel data
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}

// if pixel value is out of bounds, set it to right value from range 0-255
int normalizePixelValue(int pixelValue)
{
    if (pixelValue > RGB_MAX_COMPONENT_COLOR)
    {
        return RGB_MAX_COMPONENT_COLOR;
    }
    else if (pixelValue < 0)
    {
        return 0;
    }
    else
    {
        return pixelValue;
    }
}

void FilterPPM(PPMImage *img, int filtr[3][3], int currentAreaIndex)
{
    int areaWidth = img->x / 4;
    int areaHeight = img->y / 4;

    // calculate X and Y range of currently filtered area
    int minX = areaWidth * (currentAreaIndex % 4);
    int maxX = areaWidth * (currentAreaIndex % 4) + areaWidth - 1;

    int minY = areaHeight * (currentAreaIndex / 4);
    int maxY = areaHeight * (currentAreaIndex / 4) + areaHeight - 1;

    printf("thread ID: %d\tareaIndex: %d\tx range: %d - %d\ty range: %d - %d\n", omp_get_thread_num(), currentAreaIndex, minX, maxX, minY, maxY);
    int i, j;
    int currentPixelIndex;
    int pixelRed, pixelGreen, pixelBlue;
    int sumOfWeights = 0;

    int adjacentIndices[3][3];

    //suma skladowych filtra, tzn. suma wag przez ktora dzielona jest koncowo obliczona wartosc piksela
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            sumOfWeights += filtr[i][j];
        }
    }

    if (img)
    {

        for (i = 1; i < (img->x) - 1; i++)
        {
            if (i < minX || i > maxX)
                continue;

            for (j = 1; j < (img->y) - 1; j++)
            {
                if (j < minY || j > maxY)
                    continue;

                pixelRed = 0;
                pixelGreen = 0;
                pixelBlue = 0;

                currentPixelIndex = i * img->y + j;

                adjacentIndices[0][0] = currentPixelIndex - img->x - 1;
                adjacentIndices[0][1] = currentPixelIndex - img->x;
                adjacentIndices[0][2] = currentPixelIndex - img->x + 1;

                adjacentIndices[1][0] = currentPixelIndex - 1;
                adjacentIndices[1][1] = currentPixelIndex;
                adjacentIndices[1][2] = currentPixelIndex + 1;

                adjacentIndices[2][0] = currentPixelIndex + img->x - 1;
                adjacentIndices[2][1] = currentPixelIndex + img->x;
                adjacentIndices[2][2] = currentPixelIndex + img->x + 1;

                for (int filterRow = 0; filterRow < 3; filterRow++)
                {
                    for (int filterCol = 0; filterCol < 3; filterCol++)
                    {
                        pixelRed += filtr[filterRow][filterCol] * (img->data[adjacentIndices[filterRow][filterCol]].red);
                        pixelGreen += filtr[filterRow][filterCol] * (img->data[adjacentIndices[filterRow][filterCol]].green);
                        pixelBlue += filtr[filterRow][filterCol] * (img->data[adjacentIndices[filterRow][filterCol]].blue);
                    }
                }

                if (sumOfWeights != 0)
                {
                    pixelRed = pixelRed / sumOfWeights;
                    pixelGreen = pixelGreen / sumOfWeights;
                    pixelBlue = pixelBlue / sumOfWeights;
                }

                pixelRed = normalizePixelValue(pixelRed);
                pixelGreen = normalizePixelValue(pixelGreen);
                pixelBlue = normalizePixelValue(pixelBlue);

                img->data[currentPixelIndex].red = pixelRed;
                img->data[currentPixelIndex].green = pixelGreen;
                img->data[currentPixelIndex].blue = pixelBlue;
            }
        }
    }
}

void disableRedChannel(PPMImage *img, int currentAreaIndex)
{
    int areaWidth = img->x / 4;
    int areaHeight = img->y / 4;

    // calculate X and Y range of currently filtered area
    int minX = areaWidth * (currentAreaIndex % 4);
    int maxX = areaWidth * (currentAreaIndex % 4) + areaWidth - 1;

    int minY = areaHeight * (currentAreaIndex / 4);
    int maxY = areaHeight * (currentAreaIndex / 4) + areaHeight - 1;

    printf("thread ID: %d\tareaIndex: %d\tx range: %d - %d\ty range: %d - %d\n", omp_get_thread_num(), currentAreaIndex, minX, maxX, minY, maxY);
    int i, j;
    int currentPixelIndex;

    if (img)
    {

        for (i = 1; i < (img->x) - 1; i++)
        {
            if (i < minX || i > maxX)
                continue;

            for (j = 1; j < (img->y) - 1; j++)
            {
                if (j < minY || j > maxY)
                    continue;

                currentPixelIndex = i * img->y + j;

                img->data[currentPixelIndex].red = 0;
            }
        }
    }
}

void inverseImage(PPMImage *img, int currentAreaIndex)
{
    int areaWidth = img->x / 4;
    int areaHeight = img->y / 4;

    // calculate X and Y range of currently filtered area
    int minX = areaWidth * (currentAreaIndex % 4);
    int maxX = areaWidth * (currentAreaIndex % 4) + areaWidth - 1;

    int minY = areaHeight * (currentAreaIndex / 4);
    int maxY = areaHeight * (currentAreaIndex / 4) + areaHeight - 1;

    printf("thread ID: %d\tareaIndex: %d\tx range: %d - %d\ty range: %d - %d\n", omp_get_thread_num(), currentAreaIndex, minX, maxX, minY, maxY);
    int i, j;
    int currentPixelIndex;

    if (img)
    {

        for (i = 1; i < (img->x) - 1; i++)
        {
            if (i < minX || i > maxX)
                continue;

            for (j = 1; j < (img->y) - 1; j++)
            {
                if (j < minY || j > maxY)
                    continue;

                currentPixelIndex = i * img->y + j;

                img->data[currentPixelIndex].red = 255 - img->data[currentPixelIndex].red;
                img->data[currentPixelIndex].green = 255 - img->data[currentPixelIndex].green;
                img->data[currentPixelIndex].blue = 255 - img->data[currentPixelIndex].blue;
            }
        }
    }
}

int main(int argc, const char **argv)
{
    const char *inputFilename = NULL;

    if (argc != 2)
    {
        fprintf(stderr, "usage: filtry inputImage\n");
        return 1;
    }

    inputFilename = argv[1];

    PPMImage *image;
    image = readPPM(inputFilename);

    int areaIndex = 0;
    int myArea;

#pragma omp parallel private(myArea)
    {
        while (areaIndex < 16)
        {
#pragma omp critical
            {
                myArea = areaIndex;
                areaIndex++;
            }
            // end of critical section
            if (myArea < 16)
            {
                switch (omp_get_thread_num())
                {
                case 0:
                {
                    FilterPPM(image, sharpenFilter, myArea);
                    break;
                }
                case 1:
                {
                    FilterPPM(image, avgFilter, myArea);
                    break;
                }
                case 2:
                {
                    FilterPPM(image, LP2Filter, myArea);
                    break;
                }
                case 3:
                {
                    //FilterPPM(image, gaussFilter, myArea);
                    disableRedChannel(image, myArea);
                    break;
                }
                case 4:
                {
                    //FilterPPM(image, HP1Filter, myArea);
                    inverseImage(image, myArea);
                    break;
                }
                case 5:
                {
                    FilterPPM(image, horizontalFilter, myArea);
                    break;
                }
                case 6:
                {
                    FilterPPM(image, gradientFilter, myArea);
                    break;
                }
                case 7:
                {
                    FilterPPM(image, embossingFilter, myArea);
                    break;
                }
                }
            }
        }
    }
    // end of parallel section

    writePPM("image_filtered.ppm", image);

    free(image->data);
    free(image);

    printf("Press any key...");
    getchar();
}