#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int filters[8][3][3] = {
    //sharpening filter
    {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}},
    //horizontalFilter
    {{0, 0, 0}, {-1, 1, 0}, {0, 0, 0}},
    // avgFilter
    {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}},
    //gradientFilter
    {{-1, 1, 1}, {-1, -2, 1}, {-1, 1, 1}},
    // LP2Filter
    {{1, 1, 1}, {1, 4, 1}, {1, 1, 1}},
    // gauss filter
    {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}},
    // HP1Filter
    {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}},
    // embossingFilter
    {{-1, 0, 1}, {-1, 1, 1}, {-1, 0, 1}}};

int avgSharpen = 0;

typedef struct
{
    unsigned char red, green, blue;
} PPMPixel;

typedef struct
{
    int x, y;
    PPMPixel *data;
} PPMImage;

int Size = 800;

#define RGB_COMPONENT_COLOR 255

static PPMImage *readPPM(const char *filename)
{
    char buff[16];
    PPMImage *img;
    FILE *fp;
    int c, rgb_comp_color;
    //open PPM file for reading
    fp = fopen(filename, "rb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
    {
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6')
    {
        fprintf(stderr, "Invalid image format (must be 'P6')\n");
        exit(1);
    }

    //alloc memory form image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#')
    {
        while (getc(fp) != '\n')
            ;
        c = getc(fp);
    }

    ungetc(c, fp);
    //read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
    {
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1)
    {
        fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color != RGB_COMPONENT_COLOR)
    {
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n')
        ;
    //memory allocation for pixel data
    img->data = (PPMPixel *)malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y)
    {
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}
void writePPM(const char *filename, PPMImage *img)
{
    FILE *fp;
    //open file for output
    fp = fopen(filename, "wb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //write the header file
    //image format
    fprintf(fp, "P6\n");

    //image size
    fprintf(fp, "%d %d\n", img->x, img->y);

    // rgb component depth
    fprintf(fp, "%d\n", RGB_COMPONENT_COLOR);

    // pixel data
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}

void FilterPPM(PPMImage *img, int currentAreaIndex)
{
    int areaSize = Size / 4;
    int minX = areaSize * (currentAreaIndex % 4);
    int maxX = areaSize * (currentAreaIndex % 4) + areaSize - 1;

    int minY = areaSize * (currentAreaIndex / 4);
    int maxY = areaSize * (currentAreaIndex / 4) + areaSize - 1;

    int threadID = omp_get_thread_num();

    printf("thread ID: %d\tareaIndex: %d\tx range: %d - %d\ty range: %d - %d\n", threadID, currentAreaIndex, minX, maxX, minY, maxY);

    int i, j;
    int currentPixelIndex;
    int pixelRed, pixelGreen, pixelBlue;
    int leftIndex, rightIndex, upIndex, downIndex, leftUpIndex, rightUpIndex, leftDownIndex, rightDownIndex;
    int sharpenAvg = 0;

    //suma skladowych filtra, tzn. suma wag przez ktora dzielona jest koncowa wartosc piksela
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            sharpenAvg = sharpenAvg + filters[threadID][i][j];
        }
    }

    if (img)
    {

        for (i = 1; i < (img->x) - 1; i++)
        {
            if (i < minX || i > maxX)
                continue;

            for (j = 1; j < (img->y) - 1; j++)
            {
                if (j < minY || j > maxY)
                    continue;

                pixelRed = 0;
                pixelGreen = 0;
                pixelBlue = 0;

                currentPixelIndex = i * img->y + j;

                leftIndex = currentPixelIndex - 1;
                rightIndex = currentPixelIndex + 1;
                upIndex = currentPixelIndex - img->x;
                downIndex = currentPixelIndex + img->x;

                leftUpIndex = currentPixelIndex - img->x - 1;
                rightUpIndex = currentPixelIndex - img->x + 1;
                leftDownIndex = currentPixelIndex + img->x - 1;
                rightDownIndex = currentPixelIndex + img->x + 1;

                //printf("CurrentPixel: %d upIndex: %d leftUpIndex: %d i: %d j: %d\n",currentPixelIndex, upIndex, leftUpIndex, i, j);
                pixelRed += filters[threadID][0][0] * (img->data[leftUpIndex].red);
                pixelRed += filters[threadID][0][1] * (img->data[upIndex].red);
                pixelRed += filters[threadID][0][2] * (img->data[rightUpIndex].red);

                pixelRed += filters[threadID][1][0] * (img->data[leftIndex].red);
                pixelRed += filters[threadID][1][1] * (img->data[currentPixelIndex].red);
                pixelRed += filters[threadID][1][2] * (img->data[rightIndex].red);

                pixelRed += filters[threadID][2][0] * (img->data[leftDownIndex].red);
                pixelRed += filters[threadID][2][1] * (img->data[downIndex].red);
                pixelRed += filters[threadID][2][2] * (img->data[rightDownIndex].red);

                pixelGreen += filters[threadID][0][0] * (img->data[leftUpIndex].green);
                pixelGreen += filters[threadID][0][1] * (img->data[upIndex].green);
                pixelGreen += filters[threadID][0][2] * (img->data[rightUpIndex].green);

                pixelGreen += filters[threadID][1][0] * (img->data[leftIndex].green);
                pixelGreen += filters[threadID][1][1] * (img->data[currentPixelIndex].green);
                pixelGreen += filters[threadID][1][2] * (img->data[rightIndex].green);

                pixelGreen += filters[threadID][2][0] * (img->data[leftDownIndex].green);
                pixelGreen += filters[threadID][2][1] * (img->data[downIndex].green);
                pixelGreen += filters[threadID][2][2] * (img->data[rightDownIndex].green);

                pixelBlue += filters[threadID][0][0] * (img->data[leftUpIndex].blue);
                pixelBlue += filters[threadID][0][1] * (img->data[upIndex].blue);
                pixelBlue += filters[threadID][0][2] * (img->data[rightUpIndex].blue);

                pixelBlue += filters[threadID][1][0] * (img->data[leftIndex].blue);
                pixelBlue += filters[threadID][1][1] * (img->data[currentPixelIndex].blue);
                pixelBlue += filters[threadID][1][2] * (img->data[rightIndex].blue);

                pixelBlue += filters[threadID][2][0] * (img->data[leftDownIndex].blue);
                pixelBlue += filters[threadID][2][1] * (img->data[downIndex].blue);
                pixelBlue += filters[threadID][2][2] * (img->data[rightDownIndex].blue);

                if (sharpenAvg != 0)
                {
                    pixelRed = pixelRed / sharpenAvg;
                    pixelGreen = pixelGreen / sharpenAvg;
                    pixelBlue = pixelBlue / sharpenAvg;
                }
                if (pixelRed > 255)
                    pixelRed = 255;
                else if (pixelRed <= 0)
                    pixelRed = 0;
                if (pixelGreen > 255)
                    pixelGreen = 255;
                else if (pixelGreen <= 0)
                    pixelGreen = 0;
                if (pixelBlue > 255)
                    pixelBlue = 255;
                else if (pixelBlue <= 0)
                    pixelBlue = 0;

                img->data[currentPixelIndex].red = pixelRed;
                img->data[currentPixelIndex].green = pixelGreen;
                img->data[currentPixelIndex].blue = pixelBlue;
            }
        }
    }
}

int main()
{
    PPMImage *image;
    image = readPPM("bavaria.ppm");

    int areaIndex = 0;
    int myArea = -1;

#pragma omp parallel private(myArea)
    {
        while (areaIndex < 16)
        {
#pragma omp critical
            {
                myArea = areaIndex;
                areaIndex++;
            }
            // end of critical section
            if (myArea < 16 && myArea >= 0)
            {
                FilterPPM(image, myArea);
            }
        }
    }
    // end of parallel section

    writePPM("bavaria_filtered.ppm", image);

    //printf("Press any key...");
    //getchar();
}