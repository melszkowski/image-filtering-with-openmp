#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int sharpenFilter[3][3] = {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}};

typedef struct
{
    unsigned char red, green, blue;
} PPMPixel;

typedef struct
{
    int x, y;
    PPMPixel *data;
} PPMImage;

#define CREATOR "RPFELGUEIRAS"
#define RGB_COMPONENT_COLOR 255

static PPMImage *readPPM(const char *filename)
{
    char buff[16];
    PPMImage *img;
    FILE *fp;
    int c, rgb_comp_color;
    //open PPM file for reading
    fp = fopen(filename, "rb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //read image format
    if (!fgets(buff, sizeof(buff), fp))
    {
        perror(filename);
        exit(1);
    }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6')
    {
        fprintf(stderr, "Invalid image format (must be 'P6')\n");
        exit(1);
    }

    //alloc memory form image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#')
    {
        while (getc(fp) != '\n')
            ;
        c = getc(fp);
    }

    ungetc(c, fp);
    //read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2)
    {
        fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
        exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1)
    {
        fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
        exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color != RGB_COMPONENT_COLOR)
    {
        fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
        exit(1);
    }

    while (fgetc(fp) != '\n')
        ;
    //memory allocation for pixel data
    img->data = (PPMPixel *)malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory\n");
        exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y)
    {
        fprintf(stderr, "Error loading image '%s'\n", filename);
        exit(1);
    }

    fclose(fp);
    return img;
}
void writePPM(const char *filename, PPMImage *img)
{
    FILE *fp;
    //open file for output
    fp = fopen(filename, "wb");
    if (!fp)
    {
        fprintf(stderr, "Unable to open file '%s'\n", filename);
        exit(1);
    }

    //write the header file
    //image format
    fprintf(fp, "P6\n");

    //comments
    fprintf(fp, "# Created by %s\n", CREATOR);

    //image size
    fprintf(fp, "%d %d\n", img->x, img->y);

    // rgb component depth
    fprintf(fp, "%d\n", RGB_COMPONENT_COLOR);

    // pixel data
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}

void changeColorPPM(PPMImage *img)
{
    int i, j;
    int currentPixelIndex;
    int pixelRed, pixelGreen, pixelBlue;
    int leftIndex, rightIndex, upIndex, downIndex, leftUpIndex, rightUpIndex, leftDownIndex, rightDownIndex;
    if (img)
    {

        for (i = 1; i < (img->x) - 1; i++)
        {
            for (j = 1; j < (img->y) - 1; j++)
            {
                pixelRed = 0;
                pixelGreen = 0;
                pixelBlue = 0;
                currentPixelIndex = i * img->y + j;

                leftIndex = currentPixelIndex - 1;
                rightIndex = currentPixelIndex + 1;
                upIndex = currentPixelIndex - img->x;
                downIndex = currentPixelIndex + img->x;

                leftUpIndex = currentPixelIndex - img->x - 1;
                rightUpIndex = currentPixelIndex - img->x + 1;
                leftDownIndex = currentPixelIndex + img->x - 1;
                rightDownIndex = currentPixelIndex + img->x + 1;

                pixelRed += sharpenFilter[0][0] * (img->data[leftUpIndex].red);
                pixelRed += sharpenFilter[0][1] * (img->data[upIndex].red);
                pixelRed += sharpenFilter[0][2] * (img->data[rightUpIndex].red);

                pixelRed += sharpenFilter[1][0] * (img->data[leftIndex].red);
                pixelRed += sharpenFilter[1][1] * (img->data[currentPixelIndex].red);
                pixelRed += sharpenFilter[1][2] * (img->data[rightIndex].red);

                pixelRed += sharpenFilter[2][0] * (img->data[leftDownIndex].red);
                pixelRed += sharpenFilter[2][1] * (img->data[downIndex].red);
                pixelRed += sharpenFilter[2][2] * (img->data[rightDownIndex].red);

                pixelGreen += sharpenFilter[0][0] * (img->data[leftUpIndex].green);
                pixelGreen += sharpenFilter[0][1] * (img->data[upIndex].green);
                pixelGreen += sharpenFilter[0][2] * (img->data[rightUpIndex].green);

                pixelGreen += sharpenFilter[1][0] * (img->data[leftIndex].green);
                pixelGreen += sharpenFilter[1][1] * (img->data[currentPixelIndex].green);
                pixelGreen += sharpenFilter[1][2] * (img->data[rightIndex].green);

                pixelGreen += sharpenFilter[2][0] * (img->data[leftDownIndex].green);
                pixelGreen += sharpenFilter[2][1] * (img->data[downIndex].green);
                pixelGreen += sharpenFilter[2][2] * (img->data[rightDownIndex].green);

                pixelBlue += sharpenFilter[0][0] * (img->data[leftUpIndex].blue);
                pixelBlue += sharpenFilter[0][1] * (img->data[upIndex].blue);
                pixelBlue += sharpenFilter[0][2] * (img->data[rightUpIndex].blue);

                pixelBlue += sharpenFilter[1][0] * (img->data[leftIndex].blue);
                pixelBlue += sharpenFilter[1][1] * (img->data[currentPixelIndex].blue);
                pixelBlue += sharpenFilter[1][2] * (img->data[rightIndex].blue);

                pixelBlue += sharpenFilter[2][0] * (img->data[leftDownIndex].blue);
                pixelBlue += sharpenFilter[2][1] * (img->data[downIndex].blue);
                pixelBlue += sharpenFilter[2][2] * (img->data[rightDownIndex].blue);

                img->data[i].red = pixelRed;
                img->data[i].green = pixelGreen;
                img->data[i].blue = pixelBlue;
            }
        }
    }
}

int main()
{
    PPMImage *image;
    image = readPPM("bavaria.ppm");
    changeColorPPM(image);
    writePPM("bavaria2.ppm", image);
    printf("Press any key...");
    getchar();
}